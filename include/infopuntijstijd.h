/*
 * Zie assetlijst of topdesk
 *
 * INFOPUNTDINOTIJD [10.131.1.70]
 *
 */
#define IPSHOWCTL "10.131.1.1"
#define COMPONENT "infopuntijstijd_cpt_1"
#define UDPPORT 7000
#define UDPMSGON "INFOPUNTIJSTIJD_AAN"
#define UDPMSGOFF "INFOPUNTIJSTIJD_UIT"
