/*
 * Zie assetlijst of topdesk
 *
 * ETALAGEBEWEGING [10.130.1.71]
 *
 */
#define IPSHOWCTL "10.130.1.1"
#define COMPONENT "etalagetreintjes_cpt_1"
#define UDPPORT 7000
#define UDPMSGON "ETALAGETREINTJES_AAN"
#define UDPMSGOFF "ETALAGETREINTJES_UIT"
