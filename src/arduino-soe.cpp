/*
 * This sketch sends a UDP message over ETH from a ESP32 device to a IP adress.
 * Destination IP adress, UDP port and messages are defined in a file
 *
 * Based on the arduino-esp32
 * examples ETH_LAN8720 and WiFiUDPClient from espressif.
 *
 * https://github.com/espressif/arduino-esp32/tree/master/libraries/WiFi examples
 *
 * And Arduino's State Change Detection (Edge Detection) for pushbuttons
 * example code: https://www.arduino.cc/en/Tutorial/StateChangeDetection
 *
 */
#include <ETH.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
//#include "vars.h"

// IP address to send UDP data to:
// either use the ip address of the server or
// a network broadcast address

#define IPSHOWCTL "10.132.1.56"
#define COMPONENT "darwinvinken_cpt_2"
#define UDPPORT 7002
#define mpvstartolijfboszangervink "mpvstartolijfboszangervink"
#define mpvstartgrotegrondvink "mpvstartgrotegrondvink"
#define mpvstartkleineboomvink "mpvstartkleineboomvink"
#define mpvstartgroteboomvink "mpvstartgroteboomvink"
#define mpvstartshowespanolagrondvink "mpvstartshowespanolagrondvink"

// @todo: these values should be set via templating
const char * udpAddress = IPSHOWCTL;
const int udpPort = UDPPORT;
const char * udpMessage1 = mpvstartolijfboszangervink;
const char * udpMessage2 = mpvstartgrotegrondvink;
const char * udpMessage3 = mpvstartkleineboomvink;
const char * udpMessage4 = mpvstartgroteboomvink;
const char * udpMessage5 = mpvstartshowespanolagrondvink;

// this constant won't change:
const int  buttonPin1 = 2; // the pin that the pushbutton is attached to
const int  buttonPin2 = 3; // the pin that the pushbutton is attached to
const int  buttonPin3 = 4; // the pin that the pushbutton is attached to
const int  buttonPin4 = 5; // the pin that the pushbutton is attached to
const int  buttonPin5 = 6; // the pin that the pushbutton is attached to

// Variables will change:
int buttonPushCounter = 0; // counter for the number of button presses
// int buttonState = 0;       // current state of the button
// int lastButtonState = 0;   // previous state of the button

// long lastDebounceTime = 0;  // the last time the output pin was toggled
// long debounceDelay = 100;    // the debounce time; increase if the output flickers

//The udp library class
WiFiUDP udp;

static bool eth_connected = false;

void WiFiEvent(WiFiEvent_t event)
{
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      //set eth hostname here
      ETH.setHostname("esp32-ethernet");
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: ");
      Serial.print(ETH.macAddress());
      Serial.print(", IPv4: ");
      Serial.print(ETH.localIP());
      if (ETH.fullDuplex()) {
        Serial.print(", FULL_DUPLEX");
      }
      Serial.print(", ");
      Serial.print(ETH.linkSpeed());
      Serial.println("Mbps");
      eth_connected = true;
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("ETH Disconnected");
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("ETH Stopped");
      eth_connected = false;
      break;
    default:
      break;
  }
}

void setup()
{
  // initialize the button pin as a input:
  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);
  pinMode(buttonPin3, INPUT_PULLUP);
  pinMode(buttonPin4, INPUT_PULLUP);
  pinMode(buttonPin5, INPUT_PULLUP);

  // initialize serial communication:
  Serial.begin(9600);

  delay(1000);

  WiFi.onEvent(WiFiEvent);
  ETH.begin();

  // *** SETUP ArduinoOTA ***
  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  // ArduinoOTA.setHostname("myesp32");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

  ArduinoOTA.begin();

  Serial.println("OTA Ready");
  Serial.print("IP address: ");
  Serial.println(ETH.localIP());
}

void loop() {

  if (eth_connected) {
    //only send data when connected

    ArduinoOTA.handle();

    //filter out any noise by setting a time buffer
    // if ((millis() - lastDebounceTime) > debounceDelay) {

      // compare the buttonState to its previous state
    if (digitalRead(buttonPin1) == LOW) {
      udp.beginPacket(udpAddress,udpPort);
      Serial.println(udpMessage1);
      udp.printf(udpMessage1);
      } else {
        buttonPushCounter++;
      }
      udp.endPacket();
    if (digitalRead(buttonPin2) == LOW) {
      udp.beginPacket(udpAddress,udpPort);
      Serial.println(udpMessage2);
      udp.printf(udpMessage2);
      } else {
        buttonPushCounter++;
      }
      udp.endPacket();
    if (digitalRead(buttonPin3) == LOW) {
      udp.beginPacket(udpAddress,udpPort);
      Serial.println(udpMessage3);
      udp.printf(udpMessage3);
      } else {
        buttonPushCounter++;
      }
      udp.endPacket();
    if (digitalRead(buttonPin4) == LOW) {
      udp.beginPacket(udpAddress,udpPort);
      Serial.println(udpMessage4);
      udp.printf(udpMessage4);
      } else {
        buttonPushCounter++;
      }
      udp.endPacket();
    if (digitalRead(buttonPin5) == LOW) {
      udp.beginPacket(udpAddress,udpPort);
      Serial.println(udpMessage5);
      udp.printf(udpMessage5);
      } else {
        buttonPushCounter++;
      }
      udp.endPacket();  
        // lastDebounceTime = millis();
          // Serial.print("number of button pushes: ");
          // Serial.println(buttonPushCounter);
          // Serial.print("Seconds since boot: ");
          // Serial.println(millis()/1000);
        // Send the UDP message
      }
    }
  
